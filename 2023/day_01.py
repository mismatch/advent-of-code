EXAMPLE = '''
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
'''

example = [i for i in EXAMPLE.strip().split('\n')]
with open('day_01_input.txt') as file:
    input = [i for i in file.read().split('\n')]

# array of all digits in each line
line_digits = [[i for i in j if i.isdigit()] for j in input]
print(line_digits)

# create a 2-digit number from the first and last digit of each line
calibration_value = [int(i[0]) * 10 + int(i[-1]) for i in line_digits]
print(calibration_value)

calibration_sum = 0
for i in calibration_value:
    calibration_sum += i

print(calibration_sum)