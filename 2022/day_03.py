import string
alphabet = string.ascii_letters
print(alphabet)

with open('day_03_input.txt') as file:
    backpacks = [i for i in file.read().strip().split('\n')]

EXAMPLE='''vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw'''

example = EXAMPLE.split('\n')

# create two equal compartments (lists) for each backpack
compartments =[[i[0:len(i)//2],i[len(i)//2:]] for i in backpacks]
# print(compartments)

items = []
# find the intersections of each of the lists and print unique value
# overlap returns all items from the first list that appear in the second, so only need to add one copy of the item to the items list
for compartment in compartments:
    overlap = ([i for i in compartment[0] if i in compartment[1]])
    items.append(overlap[0])

# sum the value of each of the letters - remember that python is zero-indexed
total = 0
for i in items:
    total += alphabet.find(i) + 1

# print(items)
print(f'the total of the repeated items is {total}')

# ---------------------- PART 2 ----------------------------
# print(set(example[0]).intersection(example[1],example[2]))

# create a list of every 3 items from the backpacks list
triples = zip(*[iter(backpacks)] * 3)

# print(triples[0][2])
badges = []
for group in triples:
    backpack = [set(i) for i in group]
    overlap = backpack[0].intersection(backpack[1],backpack[2])
    badges.append(overlap)

# flatten the arrary of sets in badges into a single array using nested list comprehension
badges = [badge for i in badges for badge in i]
print(badges)

newtotal = 0
for i in badges:
    newtotal += alphabet.find(i) + 1

print(f'part 2: the total of the badges is {newtotal}')
