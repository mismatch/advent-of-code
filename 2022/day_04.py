with open('day_04_input.txt') as file:
    assignments = [i for i in file.read().strip().split('\n')]

# format each line as ['a1', 'a2', 'b1', 'b2']
times = [i.replace('-',(',')).split(',') for i in assignments]

# convert each item to a list of integers [a1, a2, b1, b2] 
times = [[int(i) for i in item] for item in times]

total = 0
for i in times:
    # bs containted in as ie a1 b1 b2 a2
    if      i[0] <= i[2] <= i[3] <= i[1] : total += 1
    # as contained in bs ie b1 a1 a2 b2
    elif    i[2] <= i[0] <= i[1] <= i[3] : total += 1

print(f'the answer to part 1 is {total}')

# ------------ part 2 ------------------ 

total2 = 0
for i in times:
    # a1 b1 a2 b2
    if      i[0] <= i[2] <= i[1] <= i[3] : total2 += 1
    # b1 a1 a2 b2
    elif    i[2] <= i[0] <= i[1] <= i[3] : total2 += 1
    # b1 a1 b2 a2
    elif    i[2] <= i[0] <= i[3] <= i[1] : total2 += 1
    # a1 b1 b2 a2
    elif    i[0] <= i[2] <= i[3] <= i[1] : total2 += 1

print(f'the answer to part 2 is {total2}')
