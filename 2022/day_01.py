EXAMPLE = '''1000
2000
3000

4000

5000
6000

7000
8000
9000

10000
'''

# create arrays of calories, each elf ends with a ''
example = [i for i in EXAMPLE.split('\n')]

with open('day_01_input.txt') as file:
    data = [i for i in file.read().strip().split('\n')]

elf_total = 0
calories = []

# for i in example:
for i in data:
    if i == '':
        calories.append(elf_total)
        elf_total = 0
    else:
        elf_total += int(i)

# must use sorted() since the sort() method returns 'none'
# calories = sorted(calories,reverse=True)

# the sort() method sorts the original list
calories.sort(reverse=True)

print(f'the full calories array: \n {calories}\n')
print(f'the maximum calories for part 1 is {max(calories)}')
print(f'the sum of the top three elves for part 2 is {sum(calories[:3])}')
