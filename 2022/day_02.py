EXAMPLE='''A Y
B X
C Z'''

with open('day_02_input.txt') as file:
    data = [i for i in file.read().strip().split('\n')]

example = EXAMPLE.split('\n')

# scores from my perspective
scores1 = {'AX': 4, 'AY': 8, 'AZ':3, 'BX': 1, 'BY':5, 'BZ': 9, 'CX':7, 'CY':2, 'CZ':6 }
scores2 = {'AX': 3, 'AY': 4, 'AZ':8, 'BX': 1, 'BY':5, 'BZ': 9, 'CX':2, 'CY':6, 'CZ':7 }

def round_score(opp_choice,my_choice,scores_array):
    return scores_array[opp_choice + my_choice]

total1 = 0
for i in data:
    total1 += round_score(i[0], i[2],scores1)

print(f'the total for method 1 is {total1}')

total2 = 0
for i in data:
    total2 += round_score(i[0], i[2],scores2)

print(f'the total for method 2 is {total2}')
