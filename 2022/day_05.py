with open('day_05_input.txt') as file:
    input = [i for i in file.read().split('\n\n')]

# split the input into starting stacks and a list of movements
stacks, moves = input[0], input[1]

# move instructions to arrays
moves = [i.split(' ') for i in moves.strip().split('\n')]
print(type(moves))
# process the initial crates
def processCrates(crates):
    '''input: list
    returns: matrix of stacks'''
    # add Xs as placeholders so matrix can be transposed
    boxes = [[j for j in i] for i in crates.replace(' ','X').split('\n')]

    # select just the columns that contain letters
    boxes = [i[1::4] for i in boxes]

    # transpose matrix of boxes
    boxes = list(map(list,zip(*boxes)))

    # remove 'X' crates
    boxes = [list(filter(lambda x: x!='X', i)) for i in boxes]

    # reverse the lists
    boxes = [i[::-1] for i in boxes]

    # drop the first element (numbers 1 to 9)
    boxes = [i[1:] for i in boxes]

    return boxes

def printStacks(stacks):
    ''' print a list of stacks, one stack per line'''
    [print(i) for i in stacks]

######### PART 1 ##################
def crateMover9000(instuction, crates):
    '''applies one line of instructions
    moves n crates from start to finish one at a time (last in first out)'''
    # select the numbers from the instuction
    n, start, finish = int(instuction[1]), int(instuction[3]), int(instuction[5])
    for _ in range(n):
        # move upto the maximum crates from the stack
        if len(crates[start-1]) > 0:
            x = crates[start-1].pop()
            crates[finish-1].append(x)
    return crates

# create the stacks input for part 1
stacks1 = processCrates(stacks)
printStacks(stacks1)

# apply each instruction from the given list of moves
for instruction in moves:
    crateMover9000(instruction,stacks1)

print('the final stacks are:')
printStacks(stacks1)

solution1 = ''.join([i[-1] for i in stacks1])
print(f'the solution to part 1 is {solution1}')

############ PART 2 ##################
def crateMover9001(instuction, crates):
    ''' applies one line of instructions
    moves n crates from start to finsh to another n at a time (maintains order)'''
    # select the numbers from the instuction
    n, start, finish = int(instuction[1]), int(instuction[3]), int(instuction[5])

    # identify crates to move
    to_move = crates[start-1][-n:]

    # remove the crates from the starting stack
    crates[start-1] = crates[start-1][:-n]

    # add the crates to the new stack
    for crate in to_move:
        crates[finish-1].append(crate)

    return crates

# create the stacks input for part 2
stacks2 = processCrates(stacks)
print('the new starting list for part 2 is')
printStacks(stacks2)

# apply each instruction from the given list of moves
for instruction in moves:
    crateMover9001(instruction,stacks2)

print('the final stacks are:')
printStacks(stacks2)

solution2 = ''.join([i[-1] for i in stacks2])
print(f'the solution to part 2 is {solution2}')
