with open('day_06_input.txt') as file:
    input = [i for i in file.read().strip()]

# count the number of unique elements in each group of 4 from the start of the list until there is a group of 4 distinct elements
for i in range(len(input)-3):
    # the set function returns an array of unique elements in the set
    group = (len(set(input[i:i+4])))
    print(group)
    if group == 4:
        print(f'the first group without repeated characters occurs after processing {i+4} characters')
        break

# alternatively, with a while loop
group = 0
j=0
while group < 4:
    # the set function returns an array of unique elements in the set
    group = (len(set(input[j:j+4])))
    j += 1

print(f'the first group without repeated characters occurs after processing {j+3} characters')

# ------------ SOLUTION ------------ 
def processChars(input,length):
    for i in range(len(input)-length-1):
        # the set function returns an array of unique elements in the set
        group = (len(set(input[i:i+length])))
        if group == length: return i + length


print(f'answer to part 1 is {processChars(input,4)}')
print(f'answer to part 2 is {processChars(input,14)}')
